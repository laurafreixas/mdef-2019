---
title: Fab Academy
layout: "page"
order: 02
---

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/e00c54508202-Screen_Shot_2015_02_16_at_12.54.58_PM.png)

### Faculty (Instructors)
- Santiago Fuentemilla: <santi@fablabbcn.org>
- Eduardo Chamorro: <eduardo.chamorro@iaac.net>
- Xavi Domínguez: <xavi@fablabbcn.org>
- Óscar González: <oscar@fablabbcn.org>

### Syllabus and Learning Objectives
The Fab Academy is a distributed educational model directed by Neil Gershenfeld of MIT’s Center For Bits and Atoms and based on MIT’s rapid prototyping course, MAS 863: How to Make (Almost) Anything. The Fab Academy began as an outreach project from the CBA, and has since spread to Fab Labs around the world. The program provides advanced digital fabrication instruction for students through an unique, hands-on curriculum and access to technological tools and resources.  
During this 6-month programme, students learn how to envision, prototype and document their projects and ideas through many hours of hands-on experience with digital fabrication tools, taking a variety of code formats and turning them into physical objects.

### Weekly Schedule
- Students view and participate in global lectures broadcasted **every Wednesdays at 9:00 am – 12:00 pm EST (3:00PM Barcelona Time**). The lectures are recorded and available to students throughout the semester.
- Students will have a **local lecture every Thursday from 10:00 AM to 14:00 PM** where the weekly lessons will be deeply explained.
- In addition to the lectures, there are 2 / 3 lab days each week where students have access the digital fabrication equipment and personal help with projects.
- Recitations happen on Mondays at the same hour as the global class, where experts in each field will talk on each weekly topic and discuss about it.

### Structure and Phases
* Jan 29: principles and practices, presentations, project management
    * Feb 03 recitation: version control
* Feb 05: computer-aided design
* Feb 12: computer-controlled cutting
    * Feb 17 recitation
* Feb 19: electronics production
* Feb 26: 3D scanning and printing
    * Mar 02 recitation
* Mar 04: electronics design
* Mar 11: computer-controlled machining
    * Mar 16 recitation
* Mar 18: embedded programming
* Mar 25: input devices
    * Mar 30 recitation
* Apr 01: applications and implications
* **Apr 08: break**
* Apr 15: output devices
    * Apr 20 recitation
* Apr 22: molding and casting
* Apr 29: networking and communications
    * May 04 recitation
* May 06: interface and application programming
* May 13: mechanical design, machine design
    * May 18 recitation
* May 20: wildcard week
* May 27: invention, intellectual property, and income
    * Jun 01 recitation
* Jun 03: project thesis development
* Jun: project presentations

### Output
Each student builds a portfolio on their respective websites that documents their mastery of different certificates taken individually along each week and their integration into a final, larger project, related to their masters thesis development.

**The Fab Academy is earned by progress rather than a global goal, for successful completion of each assignment weekly is a must.**

### Grading Method
- The student will be reviewed by turns weekly on their weekly assignmnent, on Wednesday from 3:00PM to 4:30PM.Where they will have feedback and guidance for their final master project prototype.
- Only the documentation into their webpages will be taken in account for evaluation
- The weekly standarts and grading will be presented in each weekly assignment. During the master class
- Prototyping process understanding ,workflows and evolving best practices will seriously be taken in account.


### Background Research Material
- [Links compilation](https://fabl.ink/)
- [Fab 15 Conference](https://fab15.fabevent.org/)
- [FAB Labs Community (fablabs.io)](https://www.fablabs.io/labs)
- [Academany](http://docs.academany.org/)
- [Inventory](https://docs.google.com/a/fablabbcn.org/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html)
- [Fab Foundation](https://fabfoundation.org/)
- [SCOPES DF Project](https://www.scopesdf.org/)
- [Fab Event](https://fabevent.org/)
- [Fabacademy](http://fabacademy.org/)
- [Fab Academy Staff](https://gitlab.fabcloud.org/academany/fabacademy/2019/staff)
- [Jobs](http://jobs.fabeconomy.com/)  

## Faculty Professional BIOS 

### Santiago Fuentemilla
Santiago Fuentemilla Garriga, with Master of Architecture from the University of la Salle Universitat Ramon LLull, Spain, as a specialist in Architectural Design and Construction. In 2012 he graduated from the FabAcademy Diploma at FabLab BCN, a digital fabrication and rapid prototyping course directed by Neil Gershenfeld at MIT ́s Center For Bits and Atoms (CBA). Currently, he is undertaking a PHD in digital fabrication processes at the EGA UPC (Universitat Politecnica de Catalunya). As a professional Santi has worked in various architectural firms carrying out projects at the international level in the last 10 years. He is currently the design director at OPR (Other people’s Rooms) in Barcelona, a multidisciplinary studio based on architectural concept design for enhanced user experiences. Since 2013 he is part of the Fab Lab BCN team, he is the coordinator of the Future Learning Unit (FLU), the unit focused on the design, implementation and coordination of active learning experiences with digital manufacturing tools for the community. FLU designs and promotes educational, innovation and entrepreneurship projects such as AmbMakers POPUPLAB "Digital Fabrication Everywhere", FABKIDS, CROCOPOI. FLU participates in European research projects such as DOIT or DSISCALE and PHALABS 4.0. Since 2014 he is Fab Instructor of the global academic program Fab Academy and since 2017 he is professor of the Master in Design for Emergent Futures
MDEF organized by IAAC.

### Eduardo Chamorro
Eduardo Chamorro is an architect and researcher who works towards to discover how technology can transform architecture and its processes to improve people´s lives.
He holds a Master Degree of Architecture from CEU San Pablo University (Spain), a Fab Academy diploma on Digital Fabrication offered by the Fab Lab Network and a Master Degree in Advanced Architecture from IAAC (Spain), with a specialization in digital fabrication, materiality and new design methodologies. Currently he is doing his PHD at Swinburne University of Melbourne with IaaC on HIGH-PERFORMANCE FREEFORM SPATIAL 3D PRINTING.
He holds as well a Spanish architectural licence.In addition he has worked as Fab Lab Seoul director and researcher, several architecture studios, computational design and fabrication professor at CEU University, advisor as fabrication expert for different architecture collectives,and artist, as well as common collaborator in Fab Lab Madrid and FabLab Network. 
He is always seeking to achieve an innovative architecture that attempts to solve and adapt to the society needs.
As an expert in digital fabrication. he is now teaching in MDEF, MAA2,OTF AND MAEB másters programs at IAAC.
For him working in a multi-scalar environment must be the priority in an architect nowadays.

### Xavi Domínguez
Fab Lab Barcelona - Future Learning Unit Action Researcher. [Fab Academy](https://fabacademy.org/) and [Master in Design for Emergent Futures](https://iaac.net/educational-programmes/masters-programmes/master-in-design-for-emergent-futures-mdef/) Faculty.
Xavier has a Multimedia Engineering Degree from the University of La Salle Universitat Ramon LLull, Spain and since 2016 is Fab Academy Graduated, a digital fabrication and rapid prototyping course directed by Neil Gershenfeld at MIT’s Center For Bits and Atoms (CBA).
Xavier is current maker education researcher and faculty at the Future Learning Unit. It is the Fab Lab BCN research group focused on the development of Creativity in international Educational Ecosystems. We research, design, implement and coordinate actions through creativity, experimentation, digital fabrication and coding under the concept of learning ecology.
Also he is involved in multiple EU funded research projects as [DO IT](https://www.doit-europe.net/), on entrepreneurship and social innovation for young people, [PHALABS 4.0](http://www.phablabs.eu/) that unites the photonic research and its practical application in the Fab Lab or [POP-MACHINA](https://pop-machina.eu/) that aims to demonstrate the power and potential of the maker movement and the collaborative production for the circular economy of the European Union.

### Óscar González
Óscar González is an Industrial Engineer based in Barcelona with expertise related to data analysis, testing and calibration, through his experience in the field of automotive and sensor development. He has worked for more than six years in the automotive industry for Delphi Ltd. and Nissan Motor Corp. 
As well, he has developed projects in the arts as part of a residency program in Fabra i Coats (Barcelona), collaborated with artists developing technology as well as light design for dance shows.
Currently, Óscar works as part of the Fab Lab Barcelona team doing research and development within the Smart Citizen project and is an instructor at the Fabacademy program.